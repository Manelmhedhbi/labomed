import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProfilPService} from "../profil-p.service";

@Component({
  selector: 'app-homepatient',
  templateUrl: './homepatient.component.html',
  styleUrls: ['./homepatient.component.css']
})
export class HomepatientComponent implements OnInit {

  patient:any;
  id:any=localStorage.getItem('iduser');
  patientForm:FormGroup;
  submitted=false;

  constructor(private formBuilder: FormBuilder, private pp:ProfilPService) {

    this.patient=JSON.parse(localStorage.getItem('userconnecte')!);
    this.patientForm = this.formBuilder.group({
      nom: '',
      prenom: '',
      adress: '',
      email: '',
      image: '',
      password: '',
      genre: '',
    })
  }

  ngOnInit(): void {
    this.patientForm.setValue({
      nom:this.patient.nom,
      prenom:this.patient.prenom

    })

  }
  recuper(id:any,nom:any,prenom:any, adress:any,password:any,genre:any,email:any,image:any){
    this.patient.id=id;
    this.patient.nom=nom;
    this.patient.prenom=prenom;
    this.patient.adress=adress;
    this.patient.password=password;
    this.patient.genre=genre;
    this.patient.email=email;
    this.patient.image=image;

    console.log(this.patient)
  }
  g(){
    return this.patientForm.controls;
  }
  update(){
    this.submitted = true;
    if (this.patientForm.invalid) {
      return;
    }
    console.log(this.patientForm.value)
    const formdata = new FormData();
    formdata.append('nom', this.patientForm.value.nom);
    formdata.append('prenom', this.patientForm.value.prenom);
    formdata.append('adress', this.patientForm.value.adress);
    formdata.append('email', this.patientForm.value.email);
    formdata.append('password', this.patientForm.value.password);
    formdata.append('genre', this.patientForm.value.genre);
    formdata.append('image', this.patientForm[0]);
    console.log(this.patientForm.value);
    this.pp.update(this.id,formdata).subscribe((res:any)=>{
      console.log(res);
    })
  }

}
