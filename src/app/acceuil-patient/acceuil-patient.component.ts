import { Component, OnInit,ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import  * as L from 'leaflet';
import 'mapbox-gl-leaflet';

@Component({
  selector: 'app-acceuil-patient',
  templateUrl: './acceuil-patient.component.html',
  styleUrls: ['./acceuil-patient.component.css']
})
export class AcceuilPatientComponent implements OnInit, AfterViewInit {
  @ViewChild('map')
  private mapContainer: ElementRef<HTMLElement>;
  private map: L.Map;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    const myAPIKey = "5269bdeadf16441c88feea7ee4115d61";
    const mapStyle = "https://maps.geoapify.com/v1/styles/osm-carto/style.json";

    const initialState = {
      lng: 11,
      lat: 49,
      zoom: 4
  };
    const map = new L.Map(this.mapContainer.nativeElement).setView(
      [initialState.lat, initialState.lng],
      initialState.zoom
    );
    // the attribution is required for the Geoapify Free tariff plan
    map.attributionControl
      .setPrefix("")
      .addAttribution(
        'Powered by <a href="https://www.geoapify.com/" target="_blank">Geoapify</a> | © OpenStreetMap <a href="https://www.openstreetmap.org/copyright" target="_blank">contributors</a>'
      );

    L.mapboxGL({
      style: `${mapStyle}?apiKey=${myAPIKey}`,
      accessToken: "no-token"
    }).addTo(map);
  }

}

