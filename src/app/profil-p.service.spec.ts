import { TestBed } from '@angular/core/testing';

import { ProfilPService } from './profil-p.service';

describe('ProfilPService', () => {
  let service: ProfilPService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfilPService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
