import { Component, OnInit } from '@angular/core';
import {LoginService} from "../login.service";
import {Router} from "@angular/router";
import {ProfilPService} from "../profil-p.service";

@Component({
  selector: 'app-patient-profil',
  templateUrl: './patient-profil.component.html',
  styleUrls: ['./patient-profil.component.css']
})
export class PatientProfilComponent implements OnInit {
    user:any;
    patient:any;
  fileToUpload: Array<File> = [];

  constructor(private loginService: LoginService,private route: Router, private pp: ProfilPService) {
    this.user=JSON.parse(localStorage.getItem('userconnecte')!);//! ==> can not get string null
    console.log(this.user);

  }

  ngOnInit(): void {

  }
  logout() {

    this.pp.logout().subscribe((res: any) => {
      console.log(res);
      localStorage.clear();
      this.route.navigate(['/login']);

    })
  }
  recuper(id:any,nom:any,prenom:any, email:any, password:any,image:any){
    this.patient.id=id;
    this.patient.nom=nom;
    this.patient.prenom=prenom;
    this.patient.email=email;
    this.patient.password=password;
    this.patient.image=image;
    console.log(this.patient)
  }

  recuperphoto(file:any) {

    this.fileToUpload = <Array<File>> file.target.files;
    console.log(this.fileToUpload);

  }
}
