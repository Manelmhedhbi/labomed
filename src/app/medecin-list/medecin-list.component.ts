import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import{ListMedService} from "../list-med.service";

@Component({
  selector: 'app-medecin-list',
  templateUrl: './medecin-list.component.html',
  styleUrls: ['./medecin-list.component.css']
})
export class MedecinListComponent implements OnInit {
  listMed:any=[];
  config: any;

  public maxSize: number=this.listMed.length;
  public directionLinks: boolean = true;
  public autoHide: boolean = false;
  public responsive: boolean = true;
  public labels: any = {
    previousLabel: '<--',
    nextLabel: '-->',
    screenReaderPaginationLabel: 'Pagination',
    screenReaderPageLabel: 'page'}

  constructor(private medService: ListMedService) {
    this.config = {
      itemsPerPage:2,
      currentPage: 0,
      totalItems: 5,
    }
  }

  ngOnInit(): void {
    this.getAll(this.config.currentPage)
  }
  getAll(page:any){
    this.medService.getAll(page).subscribe((res:any)=>{
      console.log(res);
      this.listMed=res['data'];

    })
  }

  pageChanged(event:any){
    console.log('current page',event);
    this.config.currentPage = event;
    this.getAll(this.config.currentPage)


  }
}
