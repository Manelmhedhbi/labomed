import {Component, OnInit, ViewChild} from '@angular/core';
import {LoginService} from "../login.service";
import Swal from 'sweetalert2';
import {Router} from "@angular/router";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  ckeditorContent: string = '<p>Some html</p>';
 @ViewChild("myckeditor") ckeditor :any;
  name = 'ng2-ckeditor';
  ckeConfig: any;
  contact={from:"",subject:"",text:"",to:"manelmhe12@gmail.com"}
  constructor(private router: Router,private loginservice:LoginService) {
  }
  config:{
    placeholder: '',
    tabsize: 2,
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo']],
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ],
    fontNames: ['Helvetica', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Roboto', 'Times']
  }
  ngOnInit(): void {
    this.ckeConfig = {
      allowedContent: true,
      extraPlugins: 'divarea,easyimage',
      forcePasteAsPlainText: true
    };
  }
  onPaste(){

  }
  onChange(){

  }

  contacter(){
    this.loginservice.contacter(this.contact).subscribe(res=>{
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your message has been sanded',
        showConfirmButton: false,
        timer: 1500
      })
      console.log(this.contact.text)
      this.router.navigate(['/patientP'])

    })
  }

}
