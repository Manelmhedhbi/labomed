import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import{environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ListMedService {

  constructor(private http: HttpClient) { }
  getAll(page:any){
    return this.http.get(environment.host+"/medecin/getall/"+page);

  }
}
