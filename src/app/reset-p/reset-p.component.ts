import { Component, OnInit } from '@angular/core';
import {LoginService} from "../login.service";
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-reset-p',
  templateUrl: './reset-p.component.html',
  styleUrls: ['./reset-p.component.css']
})
export class ResetPComponent implements OnInit {
resetForm:any;
submitted=false;
  restLink=this.routeActivated.snapshot.params.restLink
  constructor(private loginS :LoginService , private routeActivated : ActivatedRoute ,
              private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.resetForm = this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6)]],
      }
    );
  }
  get f(){
    return this.resetForm;
  }
 resetP(){
   this.submitted=true;
   if(this.resetForm.invalid){return;}
   console.log('data',this.resetForm.value);
   this.loginS.resetP({newPass:this.resetForm.value.password,resetLink:this.restLink}).subscribe((res:any)=>{
     console.log(res);
     localStorage.clear();
   this.router.navigate(['/login']);


   })
 }

}
