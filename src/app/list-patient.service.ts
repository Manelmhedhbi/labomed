import { Injectable } from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {getEntryPointInfo} from "@angular/compiler-cli/ngcc/src/packages/entry_point";

@Injectable({
  providedIn: 'root'
})
export class ListPatientService {

  constructor(private http: HttpClient) {}
  getbyid(id:any) {
    return this.http.get(environment.host + "/medecin/getbyid/"+id );
  }
  getall(page:any,size:any){
    return this.http.get(environment.host+"/patient/getallp"+page+size);
  }
}
