import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProfilMedService {

  constructor(private http: HttpClient) {
  }

  logout() {
    const token: any = localStorage.getItem('accessToken')
    const headers = new HttpHeaders({
      "x-access-token": `${token}`
    });
    return this.http.post(environment.host + '/patient/logout',
      {"refreshToken": localStorage.getItem('refToken')}, {headers: headers});
  }
}
