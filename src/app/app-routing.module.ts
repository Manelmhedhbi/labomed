import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import{NotfoundComponent} from "./home/notfound/notfound.component";
import{LoginComponent} from "./login/login.component";
import {MedecinListComponent} from "./medecin-list/medecin-list.component";
import {SecListComponent} from "./sec-list/sec-list.component";
import {ResetPComponent} from "./reset-p/reset-p.component";
import {PatientProfilComponent} from "./patient-profil/patient-profil.component";
import {AuthGuard} from "./guards/auth.guard";
import {MedecinProfilComponent} from "./medecin-profil/medecin-profil.component";
import {ContactComponent} from "./contact/contact.component";
import {HomepatientComponent} from "./homepatient/homepatient.component";
import {ListPatientComponent} from "./list-patient/list-patient.component";
import {HomeMedecinComponent} from "./home-medecin/home-medecin.component";
import {DashboComponent} from "./dashbo/dashbo.component";
import {AcceuilPatientComponent} from "./acceuil-patient/acceuil-patient.component";

const routes: Routes =
[{path:'home',component:HomeComponent,
  children:[

    {path:'medecin',component:MedecinListComponent},
    {path:'secretair',component:SecListComponent},

  ]
},{path:'login',component:LoginComponent},
  {path:'resetpassword/:restLink',component:ResetPComponent},
  {path:'patientP',component:PatientProfilComponent,canActivate:[AuthGuard],children:[
      {path:'',component:HomepatientComponent},
      {path:'contact',component:ContactComponent},
      {path:'listMed',component:MedecinListComponent},
      {path:'acceuilP',component:AcceuilPatientComponent},


    ]},// route securisé
  {path:'medecinP',component:MedecinProfilComponent,children:[
      {path:'',component:HomeMedecinComponent},
      {path:'listPatient',component:ListPatientComponent},
    ]},
  {path:'dash',component:DashboComponent},

  {path:'contact',component:ContactComponent},

  {path: '**', component: NotfoundComponent}
  ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
