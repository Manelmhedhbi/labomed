import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header/header.component';
import { FooterComponent } from './home/footer/footer.component';
import { NotfoundComponent } from './home/notfound/notfound.component';
import {LoginComponent} from "./login/login.component";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { MedecinListComponent } from './medecin-list/medecin-list.component';
import { SecListComponent } from './sec-list/sec-list.component';
import {ModalModule} from "ngx-bootstrap/modal";
import { ResetPComponent } from './reset-p/reset-p.component';
import { PatientProfilComponent } from './patient-profil/patient-profil.component';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {GoogleLoginProvider, FacebookLoginProvider} from 'angularx-social-login';
import { NgxSummernoteModule } from 'ngx-summernote';
import { MedecinProfilComponent } from './medecin-profil/medecin-profil.component';
import {CKEditorModule} from "ng2-ckeditor";
import { ContactComponent } from './contact/contact.component';
import { HomepatientComponent } from './homepatient/homepatient.component';
import {NgxPaginationModule} from "ngx-pagination";
import { ListPatientComponent } from './list-patient/list-patient.component';
import { HomeMedecinComponent } from './home-medecin/home-medecin.component';
import { DashboComponent } from './dashbo/dashbo.component';
import { AcceuilPatientComponent } from './acceuil-patient/acceuil-patient.component';

const googleLoginOptions = {
  scope: 'https://www.googleapis.com/auth/drive.metadata.readonly',
  "redirect_uri": "http://localhost/authorize/"
}; // https://developers.google.com/api-client-library/javascript/reference/referencedocs#gapiauth2clientconfig


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    NotfoundComponent,
    LoginComponent,
    MedecinListComponent,
    SecListComponent,
    ResetPComponent,
    PatientProfilComponent,
    MedecinProfilComponent,
    ContactComponent,
    HomepatientComponent,
    ListPatientComponent,
    HomeMedecinComponent,
    DashboComponent,
    AcceuilPatientComponent,


  ],
  imports: [
    BrowserModule,
    ModalModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule,
    NgxSummernoteModule,
    CKEditorModule,
    NgxPaginationModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '842960944227-18ejrbra56phac15f44ci63m4c199ieh.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    }
  ],


  bootstrap: [AppComponent]
})
export class AppModule { }
