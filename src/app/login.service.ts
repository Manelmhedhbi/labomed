import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from "@angular/common/http";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(data:any){
    return this.http.post(environment.host+"/user/authe",data);
  }
  register(data:any){
    return this.http.post(environment.host+"/user/add",data);
  }
  forgetP(data:any){
    return this.http.post(environment.host+"/user/forgetP",data);
  }
  resetP(data:any){
    return this.http.post(environment.host+"/user/resetP",data);
  }
  refT(id: any,data:any){
    const token : any = localStorage.getItem('accessToken')
    const headers = new HttpHeaders({
      "x-access-token":`${token}`
    });

    return this.http.post(environment.host+"/user/reft/"+id,{'refreshToken':data},{headers:headers});
  }

  contacter(data:any){
    return this.http.post(environment.host+"/user/sendmail",data);

  }

}
