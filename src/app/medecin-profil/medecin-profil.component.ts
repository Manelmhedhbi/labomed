import { Component, OnInit } from '@angular/core';
import {ProfilMedService} from "../profil-med.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-medecin-profil',
  templateUrl: './medecin-profil.component.html',
  styleUrls: ['./medecin-profil.component.css']
})
export class MedecinProfilComponent implements OnInit {

  constructor(private pm: ProfilMedService ,private route: Router) { }

  ngOnInit(): void {
  }
  logout() {

    this.pm.logout().subscribe((res: any) => {
      console.log(res);
      localStorage.clear();
      this.route.navigate(['/login']);

    })
  }
}
