import {Component, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../login.service";
import {Route, Router} from "@angular/router";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import {RouteConfigLoadEnd} from "@angular/router";
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  userForm: FormGroup;
  submitted = false;
  submittedL = false;
  userconnecte: any;
  iduser: any;
  modalRef: BsModalRef;
  emailForm: FormGroup;
  submittedE = false;
  user: SocialUser;
  loggedIn: boolean;
  roles:any;
  ref:any;

  constructor(private loginService: LoginService, private formBuilder: FormBuilder,
              private router: Router, private modalService: BsModalService,
              private authService: SocialAuthService) {
    this.userForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      phone: ['', [Validators.required]],
      genre: ['', [Validators.required]]

    })
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
      }
    );

    this.emailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });

    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      if(user){
      if(this.loggedIn){
        console.log(this.user);
        localStorage.setItem('userconnecte', JSON.stringify(user));
        this.router.navigate(['/patientP'])
      }}
    });

  }


  get f() {
    return this.loginForm.controls;
  }

  get g() {
    return this.userForm.controls;
  }

  get k() {
    return this.emailForm.controls;
  }

  login() {
    this.submittedL = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    console.log('data', this.loginForm.value)
    this.loginService.login(this.loginForm.value).subscribe((res: any) => {
      console.log(res);

      this.userconnecte = res.data.user;
      this.iduser = this.userconnecte._id;
      localStorage.setItem('state', '0');
      localStorage.setItem('iduser', this.iduser);
      localStorage.setItem('refToken', res['data']['refreshToken']);
      localStorage.setItem('accessToken', res['data']['accesstoken']);
      this.roles =localStorage.setItem('role', res['data'].user.role);
      console.log(this.userconnecte);
      localStorage.setItem('userconnecte', JSON.stringify(this.userconnecte));

      const jwt=res['data']['accesstoken']
      const decoded:any = jwt_decode(jwt);
      const role=decoded['role']
      if(role=='patient'){
        this.router.navigate(['/patientP'])
      }else{
      if(role=='medecin'){
        this.router.navigate(['/medecinP'])
      }}


    }, err => {
      console.log(err)
    })
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  register() {
    this.submitted = true;
    if (this.userForm.invalid) {
      return;
    }

    const formdata = new FormData();
    formdata.append('nom', this.userForm.value.nom);
    formdata.append('prenom', this.userForm.value.prenom);
    formdata.append('email', this.userForm.value.email);
    formdata.append('phone', this.userForm.value.phone);
    formdata.append('password', this.userForm.value.password);
    formdata.append('genre', this.userForm.value.genre);
    console.log('data', this.userForm.value);
    this.loginService.login(this.userForm.value).subscribe((res: any) => {
      console.log(res);
    })
  }

  forgetP() {
    this.submittedE = true;
    if (this.emailForm.invalid) {
      return;
    }
    console.log('data', this.emailForm.value);
    this.loginService.forgetP(this.emailForm.value).subscribe((res: any) => {
      console.log(res);
    })
  }

  refT(){
    this.ref=localStorage.getItem('refToken');
    this.iduser=localStorage.getItem('iduser');
    console.log('refToken',this.ref);
    console.log('iduser',this.iduser);
    this.loginService.refT(this.iduser,this.ref).subscribe((res:any)=>{
      console.log(res);
    })
  }





  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

}





