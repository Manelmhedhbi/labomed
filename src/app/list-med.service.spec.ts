import { TestBed } from '@angular/core/testing';

import { ListMedService } from './list-med.service';

describe('ListMedService', () => {
  let service: ListMedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListMedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
