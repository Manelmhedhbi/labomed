import { Component, OnInit } from '@angular/core';
import {ListPatientService} from "../list-patient.service";

@Component({
  selector: 'app-list-patient',
  templateUrl: './list-patient.component.html',
  styleUrls: ['./list-patient.component.css']
})
export class ListPatientComponent implements OnInit {

  medecinconnected:any=localStorage.getItem('iduser');
  listPatient:any=[];

  constructor( private pService: ListPatientService) {
  }

  ngOnInit(): void {
this.getbyid()  }
  getbyid() {
    this.pService.getbyid(this.medecinconnected).subscribe((res: any) => {
      console.log('medecins', res);
      this.listPatient = res

    })
  }

  getAll(page:any,size:any){
    this.pService.getall(page,size).subscribe((res:any)=>{
      console.log(res);
      this.listPatient=res['data'];})}

}
