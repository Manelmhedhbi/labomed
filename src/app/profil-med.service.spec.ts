import { TestBed } from '@angular/core/testing';

import { ProfilMedService } from './profil-med.service';

describe('ProfilMedService', () => {
  let service: ProfilMedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfilMedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
