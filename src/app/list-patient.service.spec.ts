import { TestBed } from '@angular/core/testing';

import { ListPatientService } from './list-patient.service';

describe('ListPatientService', () => {
  let service: ListPatientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListPatientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
