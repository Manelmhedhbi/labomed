import { Component, OnInit } from '@angular/core';
import {SecService} from "../sec.service";

@Component({
  selector: 'app-sec-list',
  templateUrl: './sec-list.component.html',
  styleUrls: ['./sec-list.component.css']
})
export class SecListComponent implements OnInit {
  listSec: any;

  constructor(private secService: SecService) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.secService.getAll().subscribe((res: any) => {
      console.log(res);
      this.listSec = res['data'];

    })
  }
}

